const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.sass('resources/assets/sass/page/home/home.scss', 'public/css');
mix.sass('resources/assets/sass/page/category/category.scss', 'public/css');
mix.sass('resources/assets/sass/page/detail/detail.scss', 'public/css');

mix.js('resources/assets/js/page/home.js', 'public/js');
mix.js('resources/assets/js/page/category.js', 'public/js');
mix.js('resources/assets/js/page/detail.js', 'public/js');

mix.autoload({
    jquery: ['$', 'window.jQuery', "jQuery", "window.$", "jquery", "window.jquery"]
});

