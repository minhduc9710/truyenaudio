<?php

if (!function_exists('detectDevice'))
{
    function detectDevice()
    {
        $instance = new Jenssegers\Agent\Agent();
        return $instance;
    }
}


if (!function_exists('deviceIs'))
{
    function deviceIs()
    {
        try{
            if (detectDevice()->isMobile())
            {
                return 'mobile';
            }
            else if (detectDevice()->isDesktop())
            {
                return 'desktop';
            }
            else if (detectDevice()->isTablet())
            {
                return 'tablet';
            }

            return  'desktop';
        }catch (\Exception $exception)
        {
            \Illuminate\Support\Facades\Log::error($exception->getMessage());
        }

        return  'desktop';
    }
}