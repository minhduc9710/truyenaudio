<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $device = $this->detectDevice();
        return view('pages.home.index');
    }
}
