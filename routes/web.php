<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('pages.home.index');
// });

Route::get('/', function () {
    return view('page.home.index');
});
Route::get('/detail', function () {
    return view('page.detail.index');
});
Route::get('/category', function () {
    return view('page.category.index');
});