var Comment = {
    init : function (){
        this.toggleCmtForm();
    },
    toggleCmtForm()
    {
        $('.action').on('click', '.js-form-cmt', function(){
            $(this).closest('.comment-item').find('.reply').toggle();
        });
    },
};

$( function (){
    Comment.init()
});