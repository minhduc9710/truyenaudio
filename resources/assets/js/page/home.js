import "owl.carousel/dist/owl.carousel";
import "../common/_common";
import "../common/_comment";

var Home = {
    init : function (){
        this.runSlide();
    },
    runSlide()
    {
        $('#listening-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:5
                }
            }
        });
    },
};

$( function (){
    Home.init()
});
