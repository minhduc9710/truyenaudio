import "owl.carousel/dist/owl.carousel";
import "../common/_common";
import "../common/_comment";

var Comment = {
    init : function (){
        this.runSlide();
    },
    runSlide()
    {
        $('#other-audio-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:6
                }
            }
        });
        $('#related-audio-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:6
                }
            }
        });
    },
};

$( function (){
    Comment.init()
});
