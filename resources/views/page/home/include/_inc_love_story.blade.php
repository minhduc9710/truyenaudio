<div class="box-books">
    <div class="homebox-title">
        <h3 class="header-item">Truyện ngôn tình</h3>
        <a class="view-more" title="" href="#"><i class="fa fa-external-link" aria-hidden="true"></i>&nbsp;Xem thêm</a>
    </div>
    <div class="books">
        @for ($i = 0; $i < 8; $i++)
            <div class="book-item">            
                <div class="book-item__wrap">
                    <a href="/detail" title="Trò Chơi Ma" class="product-image">
                        <img src="https://truyenaudio.org/upload/pro/Tro-Choi-Ma.jpg?quality=100&amp;mode=crop&amp;anchor=topleft&amp;width=300&amp;height=450" alt="Trò Chơi Ma">
                    </a>
                    <div class="p-name">
                        <h4>
                            <a href="/detail" title="Trò Chơi Ma">Trò Chơi Ma</a>
                        </h4>
                        <p class="tacgia">
                            <a href="/category">MC Đình Soạn</a>
                        </p>
                        <p class="audio-info">
                            <i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;00:52:02&nbsp;&nbsp;&nbsp;<i class="fa fa-list-alt" aria-hidden="true"></i> 1 phần<br>
                            <i class="fa fa-upload" aria-hidden="true"></i>&nbsp;13:57 24/05/2021
                        </p>
                    </div>
                </div>
            </div>
        @endfor
    </div>
</div>