<div class="box-listening">
    <div class="homebox-title">
        <h3 class="header-item">Đang nghe (real time)</h3>
    </div>
    <div class="listening">
        <div class="owl-carousel owl-theme" id="listening-carousel">
            @for ($i = 0; $i < 8; $i++)
                <div class="owl-book-item">
                    <a href="/detail">
                        <img alt="" src="https://truyenaudio.org/upload/pro/U-Minh-Trinh-Tham-Truyen-Linh-Di-Trinh-Tham.jpg?quality=100&amp;mode=crop&amp;anchor=topleft&amp;width=150&amp;height=0"></a>
                    <p class="useronline"><span>49</span> đang nghe</p>
                    <p class="p-name">
                        <a href="/detail" class="link-to-post">U Minh Trinh Thám - Truyện Linh Dị Trinh Thám</a>
                    </p>
                </div>
            @endfor
        </div>
    </div>
</div>