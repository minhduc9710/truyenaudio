@extends('layouts.app_default')
@section('title', 'Trang chủ')
@push('css')
    <link href="/css/home.css" rel="stylesheet">
@endpush

@section('content')
    @include('page.home.include._inc_category')
    @include('page.home.include._inc_listening')
    @include('page.home.include._inc_book_new')
    @include('page.home.include._inc_favourite')
    @include('page.home.include._inc_random_story')
    @include('page.home.include._inc_love_story')
    @include('page.home.include._inc_ghost_story')
    @include('page.home.include._inc_comic_swordplay')
    @include('components._comment')
@stop

@push('script')
    <script src="/js/home.js"></script>
@endpush
