@extends('layouts.app_default')
@section('title', 'category')
@push('css')
    <link href="/css/category.css" rel="stylesheet">
@endpush

@section('not_container')
    @include('components._breadcrumb')
@stop
@section('content')
    <div class="sort-product">
        Sắp xếp Audio
        <select name="sort">
            <option value="1">Mới nhất trước</option>
            <option value="2">Cũ nhất trước</option>
            <option value="3">Truyện ngắn nhất</option>
            <option value="4">Truyện dài nhất</option>
            <option value="5">Đánh giá tốt nhất</option>
            <option value="6">Được yêu thích nhất</option>
            <option value="7">Đánh giá nhiều nhất</option>
            <option value="8">Nghe nhiều nhất</option>
        </select>
    </div>
    <div class="box-books box-category">
        <div class="homebox-title">
            <h3 class="header-item">Được yêu thích nhất</h3>
        </div>
        <div class="books category">
            @for ($i = 0; $i < 24; $i++)
                <div class="book-item">            
                    <div class="book-item__wrap">
                        <a href="/detail" title="Trò Chơi Ma" class="product-image">
                            <img src="https://truyenaudio.org/upload/pro/Tro-Choi-Ma.jpg?quality=100&amp;mode=crop&amp;anchor=topleft&amp;width=300&amp;height=450" alt="Trò Chơi Ma">
                        </a>
                        <div class="p-name">
                            <h4>
                                <a href="/detail" title="Trò Chơi Ma">Trò Chơi Ma</a>
                            </h4>
                            <div class="rating-box">
                                <div class="ratings">
                                    <div class="rating-star" style="width: 100%"></div>
                                </div>
                                <div>
                                    <b>10</b> vote
                                </div>
                            </div>
                            <p class="audio-info">
                                <i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;00:52:02&nbsp;&nbsp;&nbsp;<i class="fa fa-list-alt" aria-hidden="true"></i> 1 phần<br>
                                <i class="fa fa-headphones" aria-hidden="true"></i>&nbsp; Lượt nghe: 18.443
                            </p>
                        </div>
                    </div>
                </div>
            @endfor
        </div>
    </div>
    {{-- @include('page.home.include._inc_category') --}}
@stop

@push('script')
    <script src="/js/category.js"></script>
@endpush
