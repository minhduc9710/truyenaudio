<div class="widgetRealTime">
    <table class="table table-striped" id="lists">
        <thead>
            <tr>
                <th>Audio cộng đồng đang nghe (Real time)
                </th>
                <th>Đang nghe
                </th>
            </tr>
        </thead>
        <tbody>
            @for ($i = 0; $i < 20; $i++)
            <tr>
                <td>
                    <a href="#">Quái Nhân Trộm Mộ</a>
                </td>
                <td>
                    <span>600</span>
                </td>
            </tr>
            @endfor
        </tbody>
    </table>
</div>