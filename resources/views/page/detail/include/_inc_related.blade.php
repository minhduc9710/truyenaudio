<div class="box-related">
    <h3 class="header-item">Truyện Audio khác</h3>
    <div class="b-related">
        <div class="related-carousel owl-carousel owl-theme" id="other-audio-carousel">
            @for ($i = 0; $i < 8; $i++)
                <div class="owl-book-item">
                    <a href="/detail">
                        <img alt="" src="https://truyenaudio.org/upload/pro/U-Minh-Trinh-Tham-Truyen-Linh-Di-Trinh-Tham.jpg?quality=100&amp;mode=crop&amp;anchor=topleft&amp;width=150&amp;height=0">
                    </a>
                    <p class="p-name">
                        <a href="/detail" class="link-to-post">U Minh Trinh Thám - Truyện Linh Dị Trinh Thám</a>
                    </p>
                </div>
            @endfor
        </div>
    </div>
</div>

<div class="box-related">
    <h3 class="header-item">Truyện Audio liên quan</h3>
    <div class="b-related">
        <div class="related-carousel owl-carousel owl-theme" id="related-audio-carousel">
            @for ($i = 0; $i < 8; $i++)
                <div class="owl-book-item">
                    <a href="/detail">
                        <img alt="" src="https://truyenaudio.org/upload/pro/U-Minh-Trinh-Tham-Truyen-Linh-Di-Trinh-Tham.jpg?quality=100&amp;mode=crop&amp;anchor=topleft&amp;width=150&amp;height=0">
                    </a>
                    <p class="p-name">
                        <a href="/detail" class="link-to-post">U Minh Trinh Thám - Truyện Linh Dị Trinh Thám</a>
                    </p>
                </div>
            @endfor
        </div>
    </div>
</div>