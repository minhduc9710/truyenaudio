<div class="box-detail">
    <div class="b-audio">
        <figure class="media-wrap">
            <a href="https://truyenaudio.org/ngon-tinh/nghe-truyen/coi-chung-tong-tai-truyen-ngon-tinh.html">
                <img alt="Coi Chừng Tổng Tài - Truyện Ngôn Tình" src="https://truyenaudio.org/upload/pro/Coi-Chung-Tong-Tai-Truyen-Ngon-Tinh.jpg?quality=100&amp;mode=crop&amp;anchor=topleft&amp;width=450&amp;height=675"></a>
            <a href="https://truyenaudio.org/ngon-tinh/nghe-truyen/coi-chung-tong-tai-truyen-ngon-tinh.html">
                <img style="margin: 10px 0" src="https://truyenaudio.org/templates/default/img/nghetruyen-icon.png"></a>
        </figure>
    </div>
    <div class="b-text">
        <section class="video-intro">
            <h1><a href="https://truyenaudio.org/ngon-tinh/nghe-truyen/coi-chung-tong-tai-truyen-ngon-tinh.html">Coi Chừng Tổng Tài - Truyện Ngôn Tình</a></h1>
            <div class="video">Video</div>
            <div class="attribute">
                    <label>Giọng đọc: </label>
                    <a href="https://truyenaudio.org/giong-doc/mc-phan-hong.html">MC Phan Hồng</a>
            </div>
            <div class="tag-book">
                <ul class="">
                    <li>
                        <a class="tag_level1 first_item" href="#" title="Truyện ngôn tình">Truyện ngôn tình</a>
                    </li> 
                    <li>
                        <a class="tag_level1 last_item" href="#" title="truyện ngắn tình yêu">truyện ngắn tình yêu</a>
                    </li>
                </ul>
            </div>
        </section>
        <ul class="related-book">                   
            <li><a href="#">Ông Xã Ngọt Ngào - Truyện Ngắn Tình Yêu</a></li>
            <li><a href="#">Tiểu Hồ Ly Của Ta - Truyện Ngôn Tình</a></li>
            <li><a href="#">Thầy Giáo Ác Quỷ - Truyện Ngôn Tình</a></li>
            <li><a href="#">Vợ Là Vợ Của Anh - Truyện Ngôn Tình</a></li>
            <li><a href="#">Hạnh Phúc Giản Đơn - Truyện Ngắn Tình Yêu</a></li>
        </ul>
    </div>
    <div class="b-ads"></div>
</div>