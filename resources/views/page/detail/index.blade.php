@extends('layouts.app_default')
@section('title', 'Detail')
@push('css')
    <link href="/css/detail.css" rel="stylesheet">
@endpush

@section('not_container')
    @include('components._breadcrumb')
@stop

@section('content')
    @include('page.detail.include._inc_detail')
    @include('components._comment')
    @include('page.detail.include._inc_table_of_contents')
    @include('page.detail.include._inc_related')
@stop

@push('script')
    <script src="/js/detail.js"></script>
@endpush
