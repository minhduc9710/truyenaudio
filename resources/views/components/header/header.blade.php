<div class="top-line">
    <div class="container">
        <a href="/category">Đăng nhập</a> | 
        <a href="/category">Đăng ký</a>
    </div>
</div>
<header class="header-top">
    <div class="container hd-top-content">
        <div class="logo">
            <a href="/">
                <img src="{{ url('images/logotruyenaudio3.png') }}" alt="Truyện Audio, truyện ngôn tình chọn lọc 2018, Mp3 320kbps chất lượng cao " data-logo-height="35">
            </a>
        </div>
        <ul class="main-menu">
            <li class="">
                <a href="/">Trang chủ</a>
            </li>
            <li>
                <a href="/category">Truyện Audio
                     <i class="fa fa-angle-down" aria-hidden="true"></i>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="/category">Tất cả Audio</a></li>
                    <li><a href="/category">Truyện ma</a></li>
                    <li><a href="/category">Truyện ngắn</a></li>
                    <li><a href="/category">Truyện dài</a></li>
                    <li><a href="/category">Tình yêu</a></li>
                    <li><a href="/category">Ngôn tình</a></li>
                    <li><a href="/category">Trinh thám</a></li>
                    <li><a href="/category">Kiếm hiệp</a></li>
                    <li><a href="/category">Lịch sử</a></li>
                    <li><a href="/category">Truyện kinh điển</a></li>
                    <li><a href="/category">Thành công - làm giàu</a></li>
                    <li><a href="/category">Nguyễn Ngọc Ngạn</a></li>
                    <li><a href="/category">Đọc truyện đêm khuya</a></li>
                    <li><a href="/category">Văn học</a></li>
                    <li><a href="/category">Radio</a></li>
                </ul>
            </li>
            <li>
                <a href="/category">Giọng đọc
                     <i class="fa fa-angle-down" aria-hidden="true"></i>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="/category">Nguyễn Ngọc Ngạn</a></li>
                    <li><a href="/category">MC Đình Soạn</a></li>
                    <li><a href="/category">Quàng A Tũn</a></li>
                    <li><a href="/category">MC Trần Vân</a></li>
                    <li><a href="/category">MC Hồng Nhung</a></li>
                    <li><a href="/category">MC Anh Tú</a></li>
                    <li><a href="/category">MC Bảo Linh</a></li>
                </ul>
            </li>
            <li>
                <a href="/category">Blog
                     <i class="fa fa-angle-down" aria-hidden="true"></i>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="/category">Người trẻ viết</a></li>
                    <li><a href="/category">Truyện đọc Nguyễn Ngọc Ngạn</a></li>
                </ul>
            </li>
            <li class="ip-search">
                <i class="fa fa-search" aria-hidden="true"></i>
            </li>
        </ul>
        <div class="mb-menu">
            <button class="btn js-mobile-panel" data-toggle="collapse" data-target=".nav-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
        </div>
    </div>
</header>
<div class="b-mega-menu">
    <div class="container mega-menu">
        <div class="col-1">
            <a href="/category">
                <img src="https://truyenaudio.org/templates/default/img/truyen-ma.png">
                Truyện ma</a>
        </div>
        <div class="col-2">
            <a href="/category">
                <img src="https://truyenaudio.org/templates/default/img/truyen-ngan.png">
                Truyện ngắn</a>
        </div>
        <div class="col-3">
            <a href="/category">
                <img src="https://truyenaudio.org/templates/default/img/trinh-tham.png">
                Trinh thám</a>
        </div>
        <div class="col-4">
            <a href="/category">
                <img src="https://truyenaudio.org/templates/default/img/kiem-hiep.png">
                Kiếm hiệp</a>
        </div>
        <div class="col-5">
            <a href="/category">
                <img src="https://truyenaudio.org/templates/default/img/truyen-dai.png">
                MC Đình Soạn</a>
        </div>
        <div class="col-6">
            <a href="/category">
                <img src="https://truyenaudio.org/templates/default/img/nguyen-ngoc-ngan.png">
                Nguyễn Ngọc Ngạn</a>
        </div>
    </div>
</div>