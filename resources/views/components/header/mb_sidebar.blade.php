<div class="mobile-panel">
    <div class="inner">
        <div class="user-control">
            <div class="user-info">
                <p>Chào mừng bạn đến với <strong>truyenaudio.com</strong></p>
                <a rel="nofollow" class="btn btn-login" href="/login">Đăng nhập</a>
                <a rel="nofollow" class="btn btn-register" href="/register">Đăng ký</a>
            </div>
        </div>
        <nav class="nav-menu">
            <ul>
                <li class="">
                    <a href="/">Trang chủ</a>
                </li>
                <li>
                    <a href="#">Truyện Audio</a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Tất cả Audio</a></li>
                        <li><a href="#">Truyện ma</a></li>
                        <li><a href="#">Truyện ngắn</a></li>
                        <li><a href="#">Truyện dài</a></li>
                        <li><a href="#">Tình yêu</a></li>
                        <li><a href="#">Ngôn tình</a></li>
                        <li><a href="#">Trinh thám</a></li>
                        <li><a href="#">Kiếm hiệp</a></li>
                        <li><a href="#">Lịch sử</a></li>
                        <li><a href="#">Truyện kinh điển</a></li>
                        <li><a href="#">Thành công - làm giàu</a></li>
                        <li><a href="#">Nguyễn Ngọc Ngạn</a></li>
                        <li><a href="#">Đọc truyện đêm khuya</a></li>
                        <li><a href="#">Văn học</a></li>
                        <li><a href="#">Radio</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Giọng đọc</a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Nguyễn Ngọc Ngạn</a></li>
                        <li><a href="#">MC Đình Soạn</a></li>
                        <li><a href="#">Quàng A Tũn</a></li>
                        <li><a href="#">MC Trần Vân</a></li>
                        <li><a href="#">MC Hồng Nhung</a></li>
                        <li><a href="#">MC Anh Tú</a></li>
                        <li><a href="#">MC Bảo Linh</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Blog</a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Người trẻ viết</a></li>
                        <li><a href="#">Truyện đọc Nguyễn Ngọc Ngạn</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
<div class="panel-backdrop js-panel-backdrop"></div>
