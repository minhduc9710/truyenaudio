<div class="grid box-comment">
    <div class="wrapper">
        <div class="content">
            <div class="comment-header">
                <div class="title">
                    Đánh giá truyện <span>(9812)</span>
                </div>
                <div class="sort">
                    <a href="#" id="sort-quantam" class="js-sort active">Hay nhất</a>
                    <a href="#" id="sort-moinhat" class="js-sort">Mới nhất</a>
                </div>
            </div>
            <div class="comment-list">
                @for ($abc = 0; $abc < 3; $abc++)
                <div class="comment-item">
                    <div class="avatar">
                        <a href="#" target="_blank">
                            <img src="https://truyenaudio.org/upload/avatar/4721.jpg?v=637318114486268979&amp;mode=crop&amp;quality=100&amp;width=70&amp;height=70" alt="Hình đại diện của Thành Nhân Nguyễn">
                        </a>
                    </div>
                    <div class="comment-right">
                        <div class="main-cmt">
                            <div class="title">
                                <a href="#" class="audioname">Linh Vũ Thiên Hạ</a>
                                <div class="name">
                                    <span><b>
                                        <a href="#" class="author" target="_blank">
                                            Thành Nhân Nguyễn</a></b></span>
                                    <span class="date-comment">03/08/2019</span>
                                </div>
                                <div class="rating-box">
                                    <div class="ratings">
                                        <div class="rating-star" style="width: 100%"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="content-cmt">
                                Chất giọng hay chuyên nghiệp
                            </div>
                    
                            <div class="action">
                                <a href="javascript:;" class="like">
                                    <i class="fa fa-thumbs-up" aria-hidden="true"></i><span>22</span>
                                    Thích
                                </a>
                                | <a href="javascript:;" class="js-form-cmt">Trả lời</a>
                                | <a href="javascript:;">Xoá</a>
                            </div>
                            <div class="reply" id="div468799">
                                <form action="/ajax/default.aspx" id="frmreply468799" novalidate="novalidate">
                                    <textarea id="comment468799" class="reply" placeholder="Ý kiến của bạn" required=""></textarea>
                                    <div class="box-btn">
                                        <button type="submit" class="btn" class="js-form-cmt">Trả lời</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @for ($i = 0; $i < 3; $i++)
                        <div class="sub-cmt">
                            <div class="avatar">
                                <a href="javascript:;" target="_blank">
                                    <img src="https://truyenaudio.org/upload/avatar/no-avatar.jpg?v=637318114501239882&amp;mode=crop&amp;quality=100&amp;width=70&amp;height=70" alt="Hình đại diện của Khách ghé thăm">
                                </a>
                            </div>
                            <div class="comment-right">
                                <div class="title">
                                    <div class="name">
                                        <span><b>
                                            <a href="javascript:;" class="author" target="_blank">Khách ghé thăm</a></b></span>
                                        <span class="date-comment">03/08/2019</span>
                                    </div>
                                </div>
                                
                                <div class="content-cmt">
                                    Truyện hay ma hay loi trang
                                </div>
                                <div class="action">
                                    <a href="javascript:;" class="like">
                                        <i class="fa fa-thumbs-up" aria-hidden="true"></i><span>22</span>
                                        Thích
                                    </a>
                                    | <a href="javascript:;" class="js-form-cmt">Trả lời</a>
                                    | <a href="javascript:;">Xoá</a>
                                </div>
                            </div>
                        </div>
                        @endfor
                    </div>        
                </div>
                @endfor
            </div>
        </div>
        <div class="sidebar sidebar-right">
            <div class="comment-post">
                <p class="cmt-title">Mời bạn đánh giá chung cho website <a href="#">Truyện Audio.Org</a></p>
                <div class="box-frm-cmt">
                    <div class="avatar">
                        <img src="https://truyenaudio.org/upload/avatar/no-avatar.png?v=637318114501269862&amp;mode=crop&amp;quality=100&amp;width=70&amp;height=70" alt="Hình đại diện của ">
                    </div>
                    <div class="frm-cmt">
                        <div class="stars">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                        </div>
                        <input type="text" name="name" id="name" placeholder="Tên hoặc bí danh" required="">
                        <textarea name="comment" id="comment" placeholder="Nhận xét về truyện audio này" rows="5" required=""></textarea>
                        <button class="btn btn-success btn-submit" type="submit">Gửi đánh giá</button>
                    </div>
                </div>
                <div style="margin-top: 20px">
                    <p style="font-style: italic">(việc bình luận, đánh giá không làm gián đoạn việc nghe truyện)</p>
                </div>
            </div>
        </div>
    </div>
</div>